# README #

This is the base code used in the project assignment of the course **Metoder och Verktyg**. Based on these implementations, the students
will create unit tests and documentation!


### How do I get set up? ###

The project uses git submodules which will need to be included recursively. Clone the project with the following command:

````
git clone --recursive git@bitbucket.org:miundsv/gameoflife_project.git
````

### Who do I talk to? ###

Erik Ström <Erik.Strom@miun.se>
